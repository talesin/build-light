﻿open System
open Talesin.BuildLight
open Talesin.BuildLight.Delcom
open Talesin.BuildLight.IO

let testDevice () = io unchecked {
    printfn "Starting..."

    let! date = releaseDate ()
    printfn "Device release date: %A" date

    let! n1 = deviceCount SearchMode.IO
    printfn "Found %i IO devices" n1

    let! n2 = deviceCount SearchMode.Visual
    printfn "Found %i visual devices" n2

    if n1 > 0 then
        let! ioname = deviceName SearchMode.IO 0
        printfn "First IO device is '%s'" ioname

    if n2 > 0 then
        let! visname = deviceName SearchMode.Visual 0
        printfn "First IO device is '%s'" visname

    let! devs = devices ()
    printfn "Found %i device(s)" devs.Length
    devs |> Array.iter (fun d -> printfn "Found device %s" d.Name)

    if devs.Length > 0 then
        let! handle = openDevice devs.[0].Name

        let turn action (c:Colour) = io unchecked {
            Console.ReadKey (false) |> ignore
            let! r = ledAction handle (action c)
            return ()
        }

        printfn "Press enter"

        do! turn On Colour.Green
        do! turn On Colour.Blue
        do! turn Flash Colour.Green
        do! turn Off Colour.Green        
        do! turn On Colour.Red
        do! turn Flash Colour.Blue
        do! turn Off Colour.Blue
        do! turn On Colour.Green
        do! turn On Colour.Blue
        do! turn Off Colour.Blue
        do! turn Flash Colour.Red
        do! turn Off Colour.Red
        do! turn Off Colour.Green

        let! x = closeDevice handle
        ()

    return ()
}

[<EntryPoint>]
let main argv =
    testDevice () |> ignore
    0 // return an integer exit code
