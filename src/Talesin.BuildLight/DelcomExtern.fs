﻿namespace Talesin.BuildLight.Extern

#nowarn "9" 

module Delcom =
    open System.Text
    open System.Runtime.InteropServices

    type uint = uint32
    
    [<StructLayout(LayoutKind.Sequential, Pack=1)>]
    type PacketStructure =
        struct
            [<DefaultValue>] val mutable Recipient: byte
            [<DefaultValue>] val mutable DeviceModel: byte
            [<DefaultValue>] val mutable MajorCmd: byte
            [<DefaultValue>] val mutable MinorCmd: byte
            [<DefaultValue>] val mutable ataLSB: byte
            [<DefaultValue>] val mutable DataMSB: byte
            [<DefaultValue>] val mutable Length: byte
            [<MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)>] 
            [<DefaultValue>] val mutable  DATA: byte[]
        end

    [<StructLayout(LayoutKind.Sequential, Pack=1)>]
    type DataExtStructure =
        struct
            [<MarshalAs( UnmanagedType.ByValArray, SizeConst = 8 )>] 
            [<DefaultValue>] val mutable  DATA: byte[]
        end

// Delcom DLL Functions - See the DelcomDLL.pdf for documentation
// http://www.delcom-eng.com/productdetails.asp?PartNumber=890510

    [<DllImport("delcomdll.dll", EntryPoint="DelcomGetDLLVersion")>]
    extern float DelcomGetDLLVersion()

    [<DllImport("delcomdll.dll", EntryPoint="DelcomVerboseControl")>]
    extern int DelcomVerboseControl(uint Mode, StringBuilder caption)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomGetDLLDate")>]
    extern int DelcomGetDLLDate(StringBuilder DateString)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomGetDeviceCount")>]
    extern int DelcomGetDeviceCount(uint ProductType)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomGetNthDevice")>]
    extern int DelcomGetNthDevice(uint ProductType, uint NthDevice, StringBuilder DeviceName)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomOpenDevice")>]
    extern uint DelcomOpenDevice(StringBuilder DeviceName, int Mode)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomCloseDevice")>]
    extern int DelcomCloseDevice(uint DeviceHandle)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomReadDeviceVersion")>]
    extern int DelcomReadDeviceVersion(uint DeviceHandle )

    [<DllImport("delcomdll.dll", EntryPoint="DelcomReadDeviceSerialNum")>]
    extern int DelcomReadDeviceSerialNum(StringBuilder DeviceName, uint DeviceHandle)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomSendPacket")>]
    extern int DelcomSendPacket(uint DeviceHandle, PacketStructure& PacketOut, PacketStructure& PacketIn)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomLEDControl")>]
    extern int DelcomLEDControl(uint DeviceHandle, int Color, int Mode)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomLoadLedFreqDuty")>]
    extern int DelcomLoadLedFreqDuty(uint DeviceHandle, byte Color, byte Low, byte High)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomEnableAutoConfirm")>]
    extern int DelcomEnableAutoConfirm(uint DeviceHandle, int Mode )
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomEnableAutoClear(")>]
    extern int DelcomEnableAutoClear(uint DeviceHandle, int Mode)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomBuzzer")>]
    extern int DelcomBuzzer(uint DeviceHandle, byte  Mode , byte Freq, byte Repeat, byte OnTime, byte OffTime)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomLoadInitialPhaseDelay")>]
    extern int DelcomLoadInitialPhaseDelay(uint DeviceHandle, byte Color, byte Delay)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomSyncLeds")>]
    extern int DelcomSyncLeds(uint DeviceHandle )
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomLoadPreScalar")>]
    extern int DelcomLoadPreScalar(uint DeviceHandle, byte PreScalar)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomGetButtonStatus")>]
    extern int DelcomGetButtonStatus(uint DeviceHandle)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomLEDPower")>]
    extern int DelcomLEDPower(uint DeviceHandle, int Color, int Power)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomWritePin")>]
    extern int DelcomWritePin(uint DeviceHandle, byte Port,  byte Pin, byte Value)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomWritePorts")>]
    extern int DelcomWritePorts(uint DeviceHandle, byte Port0,  byte Port1)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomSetupPorts")>]
    extern int DelcomSetupPorts(uint DeviceHandle, byte Port,  byte Mode0,  byte Mode1)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomReadPorts")>]
    extern int DelcomReadPorts(uint DeviceHandle, byte& Port0 , byte& Port1 )
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomWrite64Bit")>]
    extern int DelcomWrite64Bit(uint DeviceHandle, DataExtStructure& DataExt)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomRead64Bit")>]
    extern int DelcomRead64Bit(uint DeviceHandle, DataExtStructure& DataExt)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomWriteI2C")>]
    extern int DelcomWriteI2C(uint DeviceHandle, byte CmdAdd , byte Length, DataExtStructure DataExt)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomReadI2C")>]
    extern int DelcomReadI2C( uint DeviceHandle, byte CmdAdd, byte Length, DataExtStructure& DataExt)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomSelReadI2C")>]
    extern int DelcomSelReadI2C(uint DeviceHandle, byte SetAddCmd, byte Address, byte ReadCmd, byte Length , DataExtStructure& DataExt)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomReadI2CEEPROM")>]
    extern int DelcomReadI2CEEPROM( uint DeviceHandle, uint Address, uint size, byte Ctrlcode, StringBuilder& Data)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomwriteI2CEEPROM")>]
    extern int DelcomWriteI2CEEPROM( uint DeviceHandle, uint Address, uint size, byte Ctrlcode, byte WriteDelay, StringBuilder& Data)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomRS232Ctrl")>]
    extern int DelcomRS232Ctrl(uint DeviceHandle, int Mode, int Value )
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomWriteRS232")>]
    extern int DelcomWriteRS232(uint DeviceHandle , int Length, DataExtStructure& DataExt)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomReadRS232")>]
    extern int DelcomReadRS232(uint DeviceHandle, DataExtStructure& DataExt)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomSPIWrite")>]
    extern int DelcomSPIWrite(uint DeviceHandle, uint ClockCount, DataExtStructure& DataExt)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomSPISetClock")>]
    extern int DelcomSPISetClock(uint DeviceHandle, uint ClockPeriod )
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomSPIRead")>]
    extern int DelcomSPIRead(uint DeviceHandle, DataExtStructure& DataExt)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomSPIWr8Read64")>]
    extern int DelcomSPIWr8Read64(uint DeviceHandle, uint WeData, uint ClockCount, DataExtStructure& DataExt)
    
    [<DllImport("delcomdll.dll", EntryPoint="DelcomNumericMode")>]
    extern int DelcomNumericMode(uint DeviceHandle, byte Mode, byte Rate)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomNumericScanRate")>]
    extern int DelcomNumericScanRate(uint DeviceHandle, byte ScanRate)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomNumericSetup")>]
    extern int DelcomNumericSetup(uint DeviceHandle, byte Digits)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomNumericRaw")>]
    extern int DelcomNumericRaw(uint DeviceHandle, StringBuilder Str)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomNumericInteger")>]
    extern int DelcomNumericInteger(uint DeviceHandle, int Number, int Base)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomNumericHexaDecimal")>]
    extern int DelcomNumericHexaDecimal(uint DeviceHandle, int Number, int Base)

    [<DllImport("delcomdll.dll", EntryPoint="DelcomNumericDouble(")>]
    extern int DelcomNumericDouble(uint DeviceHandle, double Number, int Base)