﻿namespace Talesin.BuildLight

open System
open System.Text

 
module Delcom =
    open IO
    open Extern.Delcom

    type uint = uint32

    type SearchMode =
    | IO = 1u
    | Visual = 2u

    type Colour = | Green = 0 | Red = 1 | Blue = 2 | Yellow = 3 | Orange = 4

    type Handle =
    | Handle of uint
        member this.Value = match this with Handle(h) -> h

    type DeviceType =
    | IO of name: string
    | Visual of name: string
        member this.Name with get() = match this with IO(n) -> n | Visual(n) -> n

    type Device = 
    | Opened of handle: Handle
    | Named of name: string

    type Packet = { recipient: byte; deviceModel: byte; majorCmd: byte; minorCmd: byte; ataLSB: byte; dataMSB: byte; length: byte; data: byte[] }

    type LedControl =
    | Off of Colour
    | On of Colour
    | Flash of Colour


    /// Wraps the Delcom functions
    module Indirect =
        let private maxDeviceNameLength = 512

        let private blint b =
            if b then 1u else 0u

        let private parseDate str =
            match DateTime.TryParse str with
            | (true, date)   -> Some(date)
            | (false, _)     -> None

        let toPacketStructure (packet:Packet) =
            new PacketStructure(
                Recipient = packet.recipient,
                DeviceModel = packet.deviceModel,
                MajorCmd = packet.majorCmd,
                MinorCmd = packet.minorCmd,
                ataLSB = packet.ataLSB,
                DataMSB = packet.dataMSB,
                Length = packet.length,
                DATA = packet.data)

        let toPacket (ps:PacketStructure) =
            {   recipient = ps.Recipient;
                deviceModel = ps.DeviceModel;
                majorCmd = ps.MajorCmd;
                minorCmd = ps.MinorCmd;
                ataLSB = ps.ataLSB;
                dataMSB = ps.DataMSB;
                length = ps.Length;
                data = ps.DATA }

        /// Gets the DelcomDLL verison
        let version (extf:unit -> float) () = io unchecked {
            return extf ()
        }
        
        /// Enables verbose mode - used for debugging
        let verbose extf enable =  io expectNonZero {
            return extf (blint enable, StringBuilder())
        }

        /// Returns the Delcom DLL release date
        let releaseDate extf () = io unchecked {
            let sb = StringBuilder()
            let! result = io expectNonZero { return extf sb }
            return! IOResult.fromOption (parseDate (sb.ToString()))
        }

        /// Returns the number of devices found
        let deviceCount (extf:uint -> int) (group:SearchMode) = io unchecked {
            return extf (uint32 group)
        }

        /// Returns the name of the requested device
        let deviceName extf (group:SearchMode) (n:int) = io unchecked {
            let sb = StringBuilder(maxDeviceNameLength)
            let! result = io expectNonZero { return extf (uint32 group, uint32 n, sb) }
            return sb.ToString()
        }

        /// Returns an array of all the devices to be found
        let devices extf () = io unchecked {
            let rec loop acc mode i = io unchecked {
                let name = deviceName extf mode i
                let result = match mode, name with
                                | SearchMode.IO, Failure            -> acc, Some (SearchMode.Visual, 0)
                                | SearchMode.IO, Success n          -> ((IO n) :: acc), Some(SearchMode.IO, (i+1))
                                | SearchMode.Visual, Failure        -> acc, None
                                | SearchMode.Visual, Success n      -> ((Visual n) :: acc), Some(SearchMode.Visual, (i+1))
                                | _                                 -> failwith "unexpected mode"

                match result with
                | acc, None -> return acc
                | acc, Some(m, n) -> return! loop acc m n 
            }

            let! devices = loop [] SearchMode.IO 0 
            return devices |> List.rev |> List.toArray
        }

        /// Opens the USB device and returns a handle to it
        let openDevice (extf:(StringBuilder * int) -> uint) (name:string) = io unchecked {
            let sb = new StringBuilder(name)
            let! n = io (notExpect 0u) { return extf (sb, 0) }
            return Handle n
        }

        /// Attempts to close the USB device with the given handle
        let closeDevice extf (handle:Handle) = io expectNonZero {
            return extf (handle.Value)
        }

        /// Returns the device version for the given handle
        let deviceVersion (extf:uint -> int) (handle:Handle) = io expectNonZero {
            return extf (handle.Value)
        }

        /// Returns the device serial number for the given handle
        let deviceSerial (extf:StringBuilder * uint -> int) device = io expectNonZero {
            match device with
            | Opened(Handle(h)) ->
                return extf (null, h)
            | Named(name) ->
                let sb = new StringBuilder(name)
                return extf (sb, 0u)
        }

        /// Sends and recieves a data packet to the USB device
        let sendPacket (extf:uint * PacketStructure -> PacketStructure * int) (handle:Handle) (packet:Packet) = io unchecked {
            let! (pout, success) = io (fun (_, x) -> x = 0) {
                let mutable pin =  toPacketStructure packet
                return extf (handle.Value, pin)
            }
            
            return toPacket pout
        }

        /// Sends control messages to the device to set the LED
        let ledAction (extf:uint * int * int -> int) (handle:Handle) control = io expectZero {
            let c, m = match control with Off(c) -> c, 0 | On(c) -> c, 1 | Flash(c) -> c, 2
            return extf (handle.Value, int c, m)
        }

        // Sends message to turn LED off
        let ledTurnOff extf handle =
            (Enum.GetValues (typeof<Colour>))
            |> Seq.cast<Colour>
            |> Seq.map (fun c -> ledAction extf handle (Off c))


    /// Gets the DelcomDLL verison
    let version = Indirect.version DelcomGetDLLVersion
         
    /// Enables verbose mode - used for debugging
    let verbose = Indirect.verbose DelcomVerboseControl

    /// Returns the Delcom DLL release date
    let releaseDate = Indirect.releaseDate DelcomGetDLLDate

    /// Returns the number of devices found
    let deviceCount =  Indirect.deviceCount DelcomGetDeviceCount

    /// Returns the name of the requested device
    let deviceName = Indirect.deviceName DelcomGetNthDevice

    /// Returns an array of all the devices to be found
    let devices = Indirect.devices DelcomGetNthDevice

    /// Opens the USB device and returns a handle to it
    let openDevice = Indirect.openDevice DelcomOpenDevice

    /// Attempts to close the USB device with the given handle
    let closeDevice = Indirect.closeDevice DelcomCloseDevice

    /// Returns the device version for the given handle
    let deviceVersion = Indirect.deviceCount DelcomReadDeviceVersion

    /// Returns the device serial number for the given handle
    let deviceSerial = Indirect.deviceSerial DelcomReadDeviceSerialNum


    /// Sends and recieves a data packet to the USB device
    let sendPacket = Indirect.sendPacket (fun (h, pin) ->
                                            let mutable mpin = pin
                                            let mutable mpout = new PacketStructure()
                                            let success = DelcomSendPacket (h, &mpin, &mpout)
                                            (mpout, success))

    /// Sends control messages to the device to set the LED
    let ledAction = Indirect.ledAction DelcomLEDControl
