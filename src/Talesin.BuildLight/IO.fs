﻿namespace Talesin.BuildLight

/// Provides a return type and a simple computation expression for handling IO calls and responses
module IO =

    /// Result type from functions, allows for three different responses to an IO call
    type IOResult<'a> =
    | Success of 'a
    | Failure
    | Error of exn

    /// Helper functions
    module IOResult =
        let isSuccess io = match io with Success _ -> true | _ -> false
        let isFailure io = match io with Failure _ -> true | _ -> false
        let isError io = match io with Error _ -> true | _ -> false
        let value io = match io with Success x -> x | _ -> Unchecked.defaultof<'a>
        let fromOption opt = match opt with Some x -> Success x | _ -> Failure

    /// Computation expression
    type IOBuilder<'a> (isSuccess:'a->bool) =

        member this.Bind (value, expr) =
            match value with
            | Success x -> expr x
            | Failure -> Failure
            | Error e -> Error e

        member this.Return (value:'a) =
            if isSuccess value then
                Success value
            else
                Failure

        member this.ReturnFrom (value) =
            value

        member this.Zero () = Success ()

        member this.Combine (a, b) =
            match a with
            | Success _ -> a
            | _         -> b

        member this.Delay (f) =
            f()

    let io check = IOBuilder (check)

    let inline expect (x:'a) (y:'a) = x = y
    let inline notExpect (x:'a) (y:'a) = not (expect x y)
    let inline expectZero (x:'a) = expect x LanguagePrimitives.GenericZero
    let inline expectNonZero x = not (expectZero x)
    let inline expectOne (x:'a) = expect x LanguagePrimitives.GenericOne
    let unchecked (x:'a) = true
