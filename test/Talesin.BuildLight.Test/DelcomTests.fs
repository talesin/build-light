﻿module Talesin.BuildLightTest.DelcomTests

open System
open System.Text
open Xunit
open FsUnit.Xunit

open Talesin.BuildLight
open IO
open Delcom

[<Fact>]
let ``version should return expected number`` () =
    let expected = 1.4
    let version = Indirect.version (fun () -> expected)

    let v = version ()

    v |> should equal (Success expected)
    ()

[<Fact>]
let ``verbose enabled should use 1u`` () =
    let verbose = Indirect.verbose (fun (i, s) ->
        i |> should equal 1u
        s |> should not' (be Null)
        s.ToString() |> should be EmptyString
        1)
    
    let result = verbose true

    result |> IOResult.isSuccess |> should be True

    ()


[<Fact>]
let ``releasedate should return expected date`` () =
    let expected = DateTime(2014, 3, 10)
    let releaseDate = Indirect.releaseDate (fun sb ->
        sb.Append(expected.ToShortDateString()) |> ignore
        1)

    let result = releaseDate ()

    result |> should equal (Success expected)
    ()

[<Fact>]
let ``deviceCount for visual should return expected value`` () =
    let deviceCount = Indirect.deviceCount (fun i ->
        i |> should equal 2u
        1)
    
    let r = deviceCount SearchMode.Visual
    
    r |> should equal (Success 1)
    ()

[<Fact>]
let ``deviceCount for IO should return expected value`` () =
    let deviceCount = Indirect.deviceCount (fun i ->
        i |> should equal 1u
        1)
    
    let r = deviceCount SearchMode.IO
    
    r |> should equal (Success 1)
    ()

[<Fact>]
let ``deviceName should return expected value`` () =
    let expected = "the device name"

    let deviceName = Indirect.deviceName (fun (m, i, sb) ->
        sb.Append(expected) |> ignore
        m |> should equal 2u
        i |> should equal 0u
        1)

    let r = deviceName SearchMode.Visual 0

    r |> should equal (Success expected)
    ()

[<Fact>]
let ``devices should return expected devices`` () =
    let devices = Indirect.devices (fun (m, i, sb) ->
        match (m, i) with
        | (1u, 0u) -> sb.Append("io-one") |> ignore; 1
        | (1u, 1u) -> sb.Append("io-two") |> ignore; 1
        | (2u, 0u) -> sb.Append("vis-one") |> ignore; 1
        | (2u, 1u) -> sb.Append("vis-two") |> ignore; 1
        | _ -> 0)

    let result = devices ()

    result |> IOResult.isSuccess |> should be True

    let dev = result |> IOResult.value

    dev.Length |> should equal 4
    dev.[0] |> should equal (IO "io-one")
    dev.[1] |> should equal (IO "io-two")
    dev.[2] |> should equal (Visual "vis-one")
    dev.[3] |> should equal (Visual "vis-two")
    ()

[<Fact>]
let ``openDevice should return the success and the expected handle`` () =
    let devname = "device name"
    let openDevice = Indirect.openDevice (fun (sb, m) ->
        sb.ToString() |> should equal devname
        m |> should equal 0
        1u)

    let result = openDevice devname

    result |> IOResult.isSuccess |> should be True
    result |> should equal (IOResult<_>.Success (Handle 1u))
    ()

[<Fact>]
let ``openDevice should return failure`` () =
    let devname = "device name"
    let openDevice = Indirect.openDevice (fun (sb, m) ->
        sb.ToString() |> should equal devname
        m |> should equal 0
        0u)

    let result = openDevice devname
    result |> IOResult.isFailure |> should be True
    ()

[<Fact>]
let ``closeDevice with handle should return success`` () =
    let closeDevice = Indirect.closeDevice (fun h -> 1)

    let result = closeDevice (Handle 1u)

    result |> IOResult.isSuccess |> should be True
    result |> IOResult.value |> should equal 1
    ()

[<Fact>]
let ``closeDevice with handle should return failure`` () =
    let closeDevice = Indirect.closeDevice (fun h -> 0)

    let result = closeDevice (Handle 1u)

    result |> IOResult.isFailure |> should be True
    ()



[<Fact>]
let ``deviceVersion returns the expected value`` () =
    let deviceVersion = Indirect.deviceVersion (fun h -> 1)

    let result = deviceVersion (Handle 1u)

    result |> IOResult.isSuccess |> should be True
    result |> IOResult.value |> should equal 1
    ()


[<Fact>]
let ``deviceSerial returns the expect number from open handle`` () =
    let deviceSerial = Indirect.deviceSerial (fun (sb, ui) ->
        ui |> should equal 1u
        sb |> should be Null
        1)

    let result = deviceSerial (Opened(Handle 1u))

    result |> IOResult.isSuccess |> should be True
    result |> IOResult.value |> should equal 1
    ()

[<Fact>]
let ``deviceSerial returns the expect number from named handle`` () =
    let deviceSerial = Indirect.deviceSerial (fun (sb, ui) ->
        ui |> should equal 0u
        sb.ToString() |> should equal "the name"
        1)

    let result = deviceSerial (Named "the name")

    result |> IOResult.isSuccess |> should be True
    result |> IOResult.value |> should equal 1
    ()


[<Fact>]
let ``ledAction returns success when sending 'On' control message`` () =
    let ledAction = Indirect.ledAction (fun (h, c, m) ->
        h |> should equal 1u
        c |> should equal 4
        m |> should equal 1
        0)

    let result = ledAction (Handle 1u) (On Colour.Orange)

    result |> IOResult.isSuccess |> should be True
    ()

[<Fact>]
let ``ledAction returns success when sending 'Off' control message`` () =
    let ledAction = Indirect.ledAction (fun (h, c, m) ->
        h |> should equal 1u
        c |> should equal 4
        m |> should equal 0
        0)

    let result = ledAction (Handle 1u) (Off Colour.Orange)

    result |> IOResult.isSuccess |> should be True
    ()

[<Fact>]
let ``ledAction returns success when sending 'Flash' control message`` () =
    let ledAction = Indirect.ledAction (fun (h, c, m) ->
        h |> should equal 1u
        c |> should equal 4
        m |> should equal 2
        0)

    let result = ledAction (Handle 1u) (Flash Colour.Orange)

    result |> IOResult.isSuccess |> should be True
    ()
